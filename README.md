## STEPS TO RUN

### `git clone https://AlonsoSiux@bitbucket.org/AlonsoSiux/eresonate_contact_form.git`

this clones the public repo

### `cd eresonate_contact_form/`

In the project directory, you can run:

### `yarn install`

To install dependencies.

### `yarn start`

Runs the app in the development mode. <br/>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
