import React from 'react';
import './NavBar.sass'
import magnify from '../../assets/images/magnify.svg'
function NavBar() {
  return (
    <header className="nav-bar">
      <div className="top-center-circle"></div>
      <img src={magnify} alt="search" className="mag-glass"/>
    </header>
  )
}

export default NavBar
