import React from 'react';
import './Footer.sass'
import instagram from '../../assets/images/instagram.svg'
import linkedin from '../../assets/images/linkedin-square.svg'
import twitter from '../../assets/images/twitter-square.svg'

function Footer() {
  return (
    <footer className="footer-wrap">
      <div className="social-media">
        <img src={twitter} alt="twitter" />
        <img src={linkedin} alt="linkedin" />
        <img src={instagram} alt="instagram" />
      </div>
      <div className="footer-page-routes">
        <div className="footer-links">
          {/* map */}
          <a href="/">Claim Your Venue</a>
          <a href="/">Terms and Condition</a>
          <a href="/">Venue Log In</a>
          <a href="/">Privacy Policy</a>
        </div>
        <input className="outline-input footer-input" type="text" defaultValue="Default"/>
      </div>
    </footer>
  )
}

export default Footer
