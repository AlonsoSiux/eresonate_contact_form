import React from 'react'
import { useForm } from 'react-hook-form'
import fauxAPI from '../../api/fauxAPI'
import './Form.sass'

function Form() {
  const { register, handleSubmit, errors, reset } = useForm()
  // decided to make component a little more reusable by mapping list of attributes to reusable contact form field component
  const contactForm = [
    {
      error: errors.Name,
      name: "Name",
      label: 'Your Name',
      registerRef: register({ required: true, maxLength: 80 })
    },
    {
      error: errors.Phone,
      name: "Phone",
      label: 'Phone Number',
      inputType: 'tel',
      registerRef: register({ required: true, maxLength: 12 })
    },
    {
      error: errors.Email && errors.Email.type === 'required',
      emailError: errors.Email && errors.Email.type === 'pattern',
      inputType: 'email',
      name: 'Email',
      label: 'Email',
      registerRef: register({ required: true, pattern: /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/ })
    },
    {
      textArea: true,
      error: errors.Message,
      name: 'Message',
      label: 'Message',
      registerRef: register({ required: true })
    }
  ]

  // react-hook-form won't call onSubmit until form input is valid
  const onSubmit = async data => {
    console.log({ data })
    try {
      const newReportRes = await fauxAPI('/send_report', {
        method: 'POST',
        body: data,
      })
      console.log(newReportRes)
      alert(`${newReportRes.message}`)
    } catch (err) {
      console.log(err)
    }
    reset()
  }

  return (
    <div className="form-wrap">
      <div className="form-title">Report a Problem</div>
      <form className="form-container" onSubmit={handleSubmit(onSubmit)}>
        {contactForm.map(el => <Input key={el.name} {...el} />)}
        <input type="submit" />
      </form>
    </div>
  )
}

const Input = ({ error, emailError, registerRef, label, name, inputType = 'text', textArea = false }) => (
  <div className="flex-col relative">
    <label>
      {label}
      {
        (textArea)
        ? <textarea className="outline-input message-box" name={name} ref={registerRef} />
        : <input className="outline-input" type={inputType} name={name} ref={registerRef} />
      }
    </label>
    {error && <span className={`validation-error ${(textArea) ? 'message-error' : ''}`}>required field</span>}
    {emailError && <span className="validation-error">invalid email</span>}
  </div>
)

export default Form
