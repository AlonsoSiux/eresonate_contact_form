// function takes similar params to fetch but the response is just mocking the servers response
export default function (route, options) {

  return new Promise((resolve, reject) => {
    if (!(options.body && options.body.Name && options.body.Phone && options.body.Email && options.body.Message)) {
      setTimeout(reject({ statusCode: 400, message: 'missing fields' }), 5000)
    }
    setTimeout(resolve({ statusCode: 200, message: 'report creation success' }), 15000)
  })
}
