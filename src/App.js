import React from 'react';
// import logo from './logo.svg';
import './App.sass';
import NavBar from './components/NavBar/NavBar'
import Form from './components/Form/Form'
import Footer from './components/Footer/Footer'

function App() {
  return (
    <div className="App">
      <NavBar/>
      <Form/>
      <Footer/>
    </div>
  );
}

export default App;
